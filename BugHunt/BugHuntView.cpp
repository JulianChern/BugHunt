
// BugHuntView.cpp : CBugHuntView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "BugHunt.h"
#endif

#include "BugHuntDoc.h"
#include "BugHuntView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CBugHuntView

IMPLEMENT_DYNCREATE(CBugHuntView, CView)

BEGIN_MESSAGE_MAP(CBugHuntView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
	ON_WM_LBUTTONDOWN()
	//	ON_WM_DELETEITEM()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// CBugHuntView 构造/析构

CBugHuntView::CBugHuntView()
{
	// TODO:  在此处添加构造代码

}

CBugHuntView::~CBugHuntView()
{
}

BOOL CBugHuntView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO:  在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CBugHuntView 绘制

void CBugHuntView::OnDraw(CDC* pDC)
{
	CBugHuntDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO:  在此处为本机数据添加绘制代码

	CRect clientRect;
	GetClientRect(&clientRect);

	//获取图像高度和宽度
	int bmpW = pDoc->m_bmpBackground.GetWidth();
	int bmpH = pDoc->m_bmpBackground.GetHeight();

	//获取宽高比
	float bmpAspect = bmpH / float(bmpW);//先转换类型

	//重新计算高度和宽度
	bmpW = clientRect.Width()*0.8;
	bmpH = bmpW * bmpAspect;

	//图像位置起始坐标(left,top)
	int x = (clientRect.Width() - bmpW) / 2;
	int y = (clientRect.Height() - bmpH) / 2;

	//在内存图像中绘制
	CBitmap memBmp;//在内存中的图片
	memBmp.CreateCompatibleBitmap(pDC, clientRect.Width(), clientRect.Height());
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	memDC.SelectObject(&memBmp);

	memDC.FillSolidRect(&clientRect, RGB(255, 255, 255));//把memDC在绘制虫子之前填充为白色实矩形

	//背景青蛙
	pDoc->m_bmpBackground.StretchBlt(
		memDC.GetSafeHdc(),
		x, y,
		bmpW, bmpH
		);

	//画虫子
	for (const auto& eBug : pDoc->m_listBug) {
		if (eBug)
			eBug->Draw(&memDC);
	}

	//画青蛙
	for (const auto& ecFrog : pDoc->m_CrazyFrog){
		if (ecFrog)
			ecFrog->Draw(&memDC);
	}
	for (const auto& ehFrog : pDoc->m_HungryFrog){
		if (ehFrog)
			ehFrog->Draw(&memDC);
	}
	//显示绘制好的图像
	//将memDC内容导入到屏幕上
	pDC->BitBlt(0, 0, clientRect.Width(), clientRect.Height(),
		&memDC, 0, 0, SRCCOPY
		);

}


// CBugHuntView 打印

BOOL CBugHuntView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CBugHuntView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO:  添加额外的打印前进行的初始化过程
}

void CBugHuntView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO:  添加打印后进行的清理过程
}


// CBugHuntView 诊断

#ifdef _DEBUG
void CBugHuntView::AssertValid() const
{
	CView::AssertValid();
}

void CBugHuntView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CBugHuntDoc* CBugHuntView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CBugHuntDoc)));
	return (CBugHuntDoc*)m_pDocument;
}
#endif //_DEBUG


// CBugHuntView 消息处理程序

void CBugHuntView::OnTimer(UINT_PTR nIDEvent)
{
	CBugHuntDoc* pDoc = GetDocument();
	ASSERT(pDoc);	//检查指针合法否

	// TODO: 在此添加消息处理程序代码和/或调用默认值

	//虫子移动
	//吃虫子
	for (auto& eBug : pDoc->m_listBug) {
		//遍历，寻找是否存在与青蛙相近的虫子
		for (const auto& eFrog : pDoc->m_HungryFrog){
			if (eBug&&eFrog&&eFrog->BugIsEaten(eBug)){
				delete eBug;
				eBug = nullptr;
			}
		}
		if (eBug)
			eBug->Move();
	}
	//青蛙疯狂者移动
	for (const auto& ecFrog : pDoc->m_CrazyFrog){
		if (ecFrog)
			ecFrog->Move();
	}
	//青蛙饥饿者移动
	for (const auto& ehFrog : pDoc->m_HungryFrog){
		if (ehFrog)
			ehFrog->Move();
	}

	Invalidate();
	CView::OnTimer(nIDEvent);
}

//窗口创建中
int CBugHuntView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	//告诉虫子窗口指针
	Bug::SetParentWnd(this);//this对象访问自身地址
	SetTimer(1, 100, NULL);
	return 0;
}


//屏蔽擦除
BOOL CBugHuntView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	return TRUE;
	//return CView::OnEraseBkgnd(pDC);
}

//消灭虫子
void CBugHuntView::OnLButtonDown(UINT nFlags, CPoint point)
{
	CBugHuntDoc* pDoc = GetDocument();
	ASSERT(pDoc);	//检查指针合法否

	// TODO: 在此添加消息处理程序代码和/或调用默认值
	for (auto& eBug : pDoc->m_listBug) {
		if (eBug) {
			eBug->IsHit(point);
			if (eBug->IsDying()) {
				delete eBug;//虫子被打死，删除指针
				eBug = nullptr;
			}
		}
	}

	for (auto& ehFrog : pDoc->m_HungryFrog){
		if (ehFrog)
			ehFrog->MouseCtrlMove(point);
	}

	CView::OnLButtonDown(nFlags, point);
}


void CBugHuntView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CBugHuntDoc* pDoc = GetDocument();
	ASSERT(pDoc);	//检查指针合法否

	// TODO:  在此添加消息处理程序代码和/或调用默认值
	for (auto&ehFrog : pDoc->m_HungryFrog){
		if (ehFrog)
			ehFrog->KeyCtrlMove();
	}

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
