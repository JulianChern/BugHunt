
// BugHuntDoc.h : CBugHuntDoc 类的接口
//


#pragma once
#include"Bug.h"
#include"Frog.h"
#include<vector>


class CBugHuntDoc : public CDocument
{
	friend class CBugHuntView;
protected: // 仅从序列化创建
	CBugHuntDoc();
	DECLARE_DYNCREATE(CBugHuntDoc)

	// 特性
public:

	// 操作
public:

	// 重写
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

	// 实现
public:
	virtual ~CBugHuntDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CImage m_bmpBackground;
	std::vector<Bug*> m_listBug;
	std::vector<CrazyFrog*> m_CrazyFrog;//随机移动
	std::vector<HungryFrog*> m_HungryFrog;//鼠标，键盘控制

	// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// 用于为搜索处理程序设置搜索内容的 Helper 函数
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
