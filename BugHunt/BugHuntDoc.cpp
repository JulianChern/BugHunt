
// BugHuntDoc.cpp : CBugHuntDoc 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "BugHunt.h"
#endif

#include "BugHuntDoc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CBugHuntDoc

IMPLEMENT_DYNCREATE(CBugHuntDoc, CDocument)

BEGIN_MESSAGE_MAP(CBugHuntDoc, CDocument)
END_MESSAGE_MAP()


// CBugHuntDoc 构造/析构

CBugHuntDoc::CBugHuntDoc()
{
	// TODO:  在此添加一次性构造代码

	//青蛙
	m_bmpBackground.Load(_T(".\\Bitmap\\Frog.jpg"));
	for (int i = 0; i < 1; i++){
		CrazyFrog* pCrazyFrog = new CrazyFrog(_T(".\\Bitmap\\FrogRed.bmp"), 1, 2, 20, 40,30);
		m_CrazyFrog.push_back(pCrazyFrog);
	}
	for (int i = 0; i < 1; i++){
		HungryFrog* pHungryFrog = new HungryFrog(_T(".\\Bitmap\\FrogBlue.bmp"), 1, 2, 15, 15);
		m_HungryFrog.push_back(pHungryFrog);
	}

	//虫子
	for (int i = 0; i < 4; i++) {
		Bug* pSlowBug = new SlowBug(_T(".\\Bitmap\\RedBug96x96.bmp"), 4, 18, 10, 10, 2, 20);
		m_listBug.push_back(pSlowBug);
	}

	for (int i = 0; i < 6; i++) {
		Bug* pFastBug = new FastBug(_T(".\\Bitmap\\BlueBug36x36.bmp"), 4, 18, 10, 15, 3, 20);
		m_listBug.push_back(pFastBug);
	}
}


CBugHuntDoc::~CBugHuntDoc()
{
}

BOOL CBugHuntDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO:  在此添加重新初始化代码
	// (SDI 文档将重用该文档)

	return TRUE;
}




// CBugHuntDoc 序列化

void CBugHuntDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO:  在此添加存储代码
	}
	else
	{
		// TODO:  在此添加加载代码
	}
}

#ifdef SHARED_HANDLERS

// 缩略图的支持
void CBugHuntDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// 修改此代码以绘制文档数据
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 搜索处理程序的支持
void CBugHuntDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// 从文档数据设置搜索内容。
	// 内容部分应由“;”分隔

	// 例如:     strSearchContent = _T("point;rectangle;circle;ole object;")；
	SetSearchContent(strSearchContent);
}

void CBugHuntDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CBugHuntDoc 诊断

#ifdef _DEBUG
void CBugHuntDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CBugHuntDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CBugHuntDoc 命令
