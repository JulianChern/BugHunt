#pragma once
#include "Sprite.h"
#include "Bug.h"
#include<vector>

class Frog : public Sprite
{
public:
	Frog(const CString& strBitmapFile, int nRow, int nCol,
		int nMoveStep, int nSpeed);
	virtual ~Frog(){}

	virtual void Move() = 0;


protected:
	int m_iCurrentDir;      //the current moving direction
	int m_nSpeed;           //the moving speed of bug (the time to refresh, Unite: ms) 
};


//CrazyFrog
class CrazyFrog : public Frog
{
public:
	CrazyFrog(const CString& strBitmapFile, int nRow, int nCol,
		int nMoveStep, int nSpeed, int dirChangeProb = 80) :Frog(strBitmapFile, nRow, nCol, nMoveStep, nSpeed), m_nDirChangeProb(dirChangeProb){}

	~CrazyFrog(){}

	virtual void Move();
	void ChangeDirection();

private:
	int m_nDirChangeProb;   //the probs of the bug changing direction
};


//HungryFrog
class HungryFrog : public Frog
{
public:
	HungryFrog(const CString& strBitmapFile, int nRow, int nCol,
		int nMoveStep, int nSpeed) :Frog(strBitmapFile, nRow, nCol, nMoveStep, nSpeed){
		m_nStepX = m_nMoveStep;//Ĭ�������ƶ�
	}

	~HungryFrog(){}

	virtual void Move();

	bool BugIsEaten(Bug*& pBug);
	void KeyCtrlMove();
	void MouseCtrlMove(CPoint PTM);
};