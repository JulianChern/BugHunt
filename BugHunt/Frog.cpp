#include "stdafx.h"
#include "Frog.h"
#include<cmath>
//宏定义 按键状态真值
#define UP (::GetKeyState(VK_UP) < 0 || ::GetKeyState('W') < 0 || ::GetKeyState('w') <0)
#define DOWN (::GetKeyState(VK_DOWN) < 0 || ::GetKeyState('S') < 0 || ::GetKeyState('s') <0)
#define LEFT (::GetKeyState(VK_LEFT) < 0 || ::GetKeyState('A') < 0 || ::GetKeyState('a') <0)
#define RIGHT (::GetKeyState(VK_RIGHT) < 0 || ::GetKeyState('D') < 0 || ::GetKeyState('d') <0)


Frog::Frog(const CString& strBitmapFile, int nRow, int nCol,
	int nMoveStep, int nSpeed)
{
	LoadImage(strBitmapFile, nRow, nCol);

	m_nMoveStep = nMoveStep;
	m_nSpeed = nSpeed;
	m_iCurrentDir = 0;
}

//CrazyFrog Function Loading
void CrazyFrog::Move()
{
	ChangeDirection();
	m_rcSprite.OffsetRect(m_nStepX, m_nStepY);

	if (AtLeftEdge() | AtRightEdge())
	{
		m_nStepX = -m_nStepX;
	}
	if (AtTopEdge() | AtBottomEdge())
	{
		m_nStepY = -m_nStepY;
	}

}

void CrazyFrog::ChangeDirection(){
	int nProb = rand() % 100;
	if (nProb < m_nDirChangeProb){
		int nDeg = rand() % 360;
		if (nDeg == 90 || nDeg == 270){}
		else if ((nDeg >= 0 && nDeg < 90) || (nDeg > 270 && nDeg <= 360)){
			SetPictureIdx(0);
		}
		else
			SetPictureIdx(1);

		m_nStepX = m_nMoveStep*cos(nDeg*PI / 180);
		m_nStepY = -m_nMoveStep*sin(nDeg*PI / 180);

	}
}


//HungryFrog Function Loading
void HungryFrog::KeyCtrlMove(){
	int boL[4] = { LEFT, UP, RIGHT, DOWN };		//按键状态真值表
	if (LEFT || RIGHT)		//仅当左右键按下，才改变青蛙方向
		SetPictureIdx(boL[0]);
	//重新计算步数，当左右或上下键同时被按下，青蛙停止移动
	m_nStepX = -boL[0] * m_nMoveStep + boL[2] * m_nMoveStep;
	m_nStepY = -boL[1] * m_nMoveStep + boL[3] * m_nMoveStep;
	//当左右键之一以及上下键之一被同时按下，x，y上的投影应是上式除以根号2
	if (((boL[0] + boL[2]) == 1) && ((boL[1] + boL[3]) == 1)){
		m_nStepX /= sqrt(2.0);
		m_nStepY /= sqrt(2.0);
	}
}

void HungryFrog::MouseCtrlMove(CPoint PTM){
	//计算鼠标坐标与青蛙坐标距离
	float Dx = PTM.x - m_rcSprite.CenterPoint().x;
	float Dy = PTM.y - m_rcSprite.CenterPoint().y;
	float Dd = sqrt(Dx*Dx + Dy*Dy);
	if (Dx > 0)
		SetPictureIdx(0);
	else if (Dx < 0)
		SetPictureIdx(1);
	m_nStepX = m_nMoveStep*Dx / Dd;
	m_nStepY = m_nMoveStep*Dy / Dd;
}

void HungryFrog::Move(){
	m_rcSprite.OffsetRect(m_nStepX, m_nStepY);

	CRect rectClient;
	m_pParentWnd->GetClientRect(rectClient);

	int xOff = rectClient.Width() - m_rcSprite.Width() - 1;
	int yOff = rectClient.Height() - m_rcSprite.Height() - 1;
	if (AtLeftEdge())
	{
		m_rcSprite.OffsetRect(xOff, 0);
	}
	if (AtTopEdge())
	{
		m_rcSprite.OffsetRect(0, yOff);
	}
	if (AtRightEdge())
	{
		m_rcSprite.OffsetRect(-xOff, 0);
	}
	if (AtBottomEdge())
	{
		m_rcSprite.OffsetRect(0, -yOff);
	}
}

bool HungryFrog::BugIsEaten(Bug*& pBug){
	//设定虫子被吃最小距离
	int minD = (min(m_rcSprite.Width(), m_rcSprite.Height()) + min(pBug->m_rcSprite.Width(), pBug->m_rcSprite.Height()))*0.5;
	//青蛙与虫子距离在x、y上的投影
	int Dx = this->m_rcSprite.CenterPoint().x - pBug->m_rcSprite.CenterPoint().x;
	Dx = Dx > -Dx ? Dx : -Dx;
	int Dy = this->m_rcSprite.CenterPoint().y - pBug->m_rcSprite.CenterPoint().y;
	Dy = Dy > -Dy ? Dy : -Dy;

	//以Dx+Dy代替青蛙与虫子距离，以提升效率
	return (Dx + Dy) < minD ? TRUE : FALSE;
}